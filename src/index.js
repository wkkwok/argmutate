export const mutate = (...mutators) => {
	return function(target, key, descriptor) {
		let method = descriptor.value;
		descriptor.value = function(...args) {
			let mutatedArgs = [];
			for (let i = 0; i < args.length; i++) {
				let arg = args[i];
				let mutatedArg = i < mutators.length
					? mutators[i] instanceof Function
						? mutators[i](arg)
						: arg
					: arg;
				mutatedArgs.push(mutatedArg);
			}
			method.apply(target, mutatedArgs);
		}
	}
}

export const combine = (combiner, split = false) => {
	return function(target, key, descriptor) {
		let method = descriptor.value;
		descriptor.value = function(...args) {
			let combinedArg = combiner(...args);
			if (typeof split === 'boolean' && split && combinedArg.constructor == Array) {
				method.call(target, ...combinedArg);
			} else {
				method.call(target, combinedArg);
			}

		}
	}
}

export const arrayify = arg => {
	return arg || arg === false || arg == '' || arg == 0
		? arg instanceof Array
			? arg
			: [arg]
		: [];
}

export const objectify = arg => {
	return arg || arg === false || arg == '' || arg == 0
		? arg instanceof Object
			? arg
			: { value : arg }
		: {};
}
